import { useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
// import Router from 'next/router'
import Link from 'next/link';
import Head from 'next/head'
import Swal from 'sweetalert2'

import UserContext from '../UserContext';

export default function NavBar(){
	const { user } = useContext(UserContext);

	return(
        <React.Fragment>
            <Head>
                <script src="https://kit.fontawesome.com/a65a62c6bd.js" crossorigin="anonymous"></script>
            </Head>
            {user.email 
            ?   <React.Fragment>
                <Navbar bg="light" expand="lg" fixed="top">
                <Link href="/welcome">
                    <a className="navbar-brand">iBudget</a>
                </Link>
                    <Nav className="ml-auto">
                        <Link href="/logout">
                            <a className="navbar-brand" role="button"><i class="fas fa-sign-out-alt fa-lg"></i></a>
                        </Link>
                    </Nav>
                </Navbar>
                <Navbar bg="light" expand="lg" fixed="bottom" className="w-100 text-center d-flex justify-content-center">

                    <Link href="/welcome">
                        <a className="nav-link icons"><i class="fas fa-home"></i></a>
                    </Link>
                    <Link href="/categories">
                        <a className="nav-link icons"><i class="fas fa-list-alt"></i></a>
                    </Link>
                    <Link href="/transactions">
                        <a className="nav-link icons"><i class="fas fa-receipt"></i></a>
                    </Link>
                    <Link href="/overview">
                        <a className="nav-link icons"><i class="fas fa-chart-bar"></i></a>
                    </Link> 
                </Navbar>
                </React.Fragment>
            :   <Navbar bg="light" variant="light">
                    <Navbar.Brand href="#home">
                      Your Daily Budget Guide
                    </Navbar.Brand>
                </Navbar>
            }     
        </React.Fragment>
	)
}