import { useState, useEffect } from 'react';
import { Card, Button, Row, Col } from 'react-bootstrap'
import Router from 'next/router'
import AppHelper from '../apphelper'
// import View from '../components/View'
import moment from 'moment'
import Swal from 'sweetalert2'
import welcomes from '../styles/Welcome.module.css'

export default function welcome(){
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [balance, setBalance] = useState(0)
    const [date, setDate] = useState('')
    const [totalExpense, setTotalExpense] = useState(0)

    useEffect(() => {
        fetch(`${ AppHelper.API_URL }/users/details`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setFirstName(data.firstName)
            setLastName(data.lastName)
        })
    }, [firstName, lastName])

    useEffect(() => {
        fetch(`${ AppHelper.API_URL }/users/get-records`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data.length > 0){
                setBalance(data[data.length-1].balanceAfterTransaction)
                setDate(moment(data[data.length-1].dateAdded).format('LL'))

                let totalExpense = 0;

                let transactionArr = data.map(transaction => {
                    if(transaction.type === "Expenses"){
                        totalExpense = totalExpense + transaction.amount   
                    }
                })
                setTotalExpense(totalExpense)                
            }else{
                Swal.fire({
                  icon: 'error',
                  title: 'Oops... Something went wrong!',
                  text: 'Your probably not logged in or you do not have transactions yet!',
                  footer: '<a href="/register">Please click me to Register/Login or OK to create transactions.</a>',
                  allowOutsideClick: false
                })
                Router.push('/categories')
            }
        })
    }, [balance, date, totalExpense])

    return (
        <React.Fragment>     
            <body className={welcomes.welcome}>   
            <Card>
                <Card.Header className="text-center">Welcome, {firstName}!</Card.Header>
                <Card.Body>
                    <Card className="text-center">
                      <Card.Body>
                        <Card.Title>Your available Balance as of {date}</Card.Title>
                      </Card.Body>
                      <Card.Footer className="text-muted">&#8369; {balance}</Card.Footer>
                    </Card>
                    <Card className="text-center mt-3">
                      <Card.Body>
                        <Card.Title>Your Total Expenses</Card.Title>
                      </Card.Body>
                      <Card.Footer className="text-muted">&#8369; {totalExpense}</Card.Footer>
                    </Card>
                </Card.Body>
            </Card>
            </body>
        </React.Fragment>     
    )
}