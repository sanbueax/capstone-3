import { useEffect, useState } from 'react'
import moment from 'moment'
import { Tabs, Tab, Card } from 'react-bootstrap'
import BarChart from '../components/BarChart'
import AppHelper from '../apphelper'
import styles from '../styles/Overview.module.css'
// import Head from 'next/head'

export default function overview(){
	const [income, setIncome] = useState([])
	const [expenses, setExpenses] = useState([])
	// const [records, setRecords] = useState([])

	useEffect(() => {
		fetch(`${ AppHelper.API_URL }/users/get-records`, {
			headers: {
	            'Authorization': `Bearer ${localStorage.getItem('token')}`
	        }
		})
		.then(res => res.json())
		.then(data => {
			// setRecords(data)
			if(data.length > 0){
				let monthlyIncome = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				let monthlyExpenses = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

				data.forEach(transaction => {
					const index = moment(transaction.dateAdded).month()

					if(transaction.type === "Income"){
	                    monthlyIncome[index] += (parseInt(transaction.amount))   
	                }else{
	                	monthlyExpenses[index] += (parseInt(transaction.amount))
	                }
				})

				setIncome(monthlyIncome)
				setExpenses(monthlyExpenses)
			}			
		})
	}, [])

	return(
		<React.Fragment>
		<body className={styles.main}>
		<Card>
			<Card.Body>		
				<Tabs defaultActiveKey="monthly" id="monthlyFigures" className="mb-3">
					<Tab eventKey="monthly" title="Monthly Income and Expenses">
						<BarChart figuresArray={income} label={"Total Monthly Income"}/>
						<hr/>
						<BarChart figuresArray={expenses} label={"Total Monthly Expenses"}/>
					</Tab>
				</Tabs>
			</Card.Body>
		</Card>
		</body>
		</React.Fragment>
	)
}