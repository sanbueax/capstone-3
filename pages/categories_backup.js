import { useState, useEffect } from 'react';
import { Card, Form, ListGroup, FormControl, Button } from 'react-bootstrap'
import Router from 'next/router'
import AppHelper from '../apphelper'
import moment from 'moment'

export default function categories(){

    const [categories, setCategories] = useState('')
    const [categoryType, setCategoryType] = useState('All')
    const [list, setList] = useState('')
    const [search, setSearch] = useState('')
    const [searchData, setSearchData] = useState('')

    useEffect(() => {
    	fetch(`${ AppHelper.API_URL }/users/get-records`, {
    		method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			data.sort((a, b) => {
			    if(a.dateAdded < b.dateAdded){
			      return 1
			    }else if(a.dateAdded > b.dateAdded){
			      return -1
			    }else{
			      return 0
			    }
			})
			setCategories(data)
		})
    }, [categories])

    useEffect(() => {
    	if(search !== ""){
    		let term = new RegExp(search)
    		// console.log(term)
    		setSearchData(categories.filter(record => {
    			console.log(record)
    			console.log(term.test(record.categoryName.toLowerCase()))
    			console.log(term.test(record.balanceAfterTransaction.toString()))
    			console.log(term.test(record.description.toLowerCase()))
    			console.log(term.test(record.amount.toString()))
    			console.log(term.test(record.dateAdded.toString()))
    			return (term.test(record.categoryName.toLowerCase())
    				|| term.test(record.description.toLowerCase())
    				|| term.test(record.amount.toString())
    				|| term.test(record.balanceAfterTransaction.toString())
    				|| term.test(record.dateAdded.toString()))
    		}))
    	}else{
    		setSearchData(categories)
    	}
    }, [search, categories])

    useEffect(() => {
    	let searchDataArray;
    	if(searchData.length > 0){
			if(categoryType === "All"){
				searchDataArray = searchData.filter(name => {
					// console.log(name)
					return name.type === name.type
				})
				setList(searchDataArray.map(transaction => {
					return(
						<React.Fragment key={transaction._id}>
							<Card.Header>{transaction.categoryName} ({transaction.type})</Card.Header>
							<Card.Body>
								{transaction.type === "Income" 
								? <p>Added Amount: {transaction.amount}</p> 
								: <p>Amount Spent: {transaction.amount}</p>}
							    <p>Description: {transaction.description}</p>
							    <p>Balance: &#8369; {transaction.balanceAfterTransaction}</p>
							    <p>Transaction Date: {moment(transaction.dateAdded).format('LLL')}</p>
							</Card.Body>
						</React.Fragment>
					)
				}))
			}else if(categoryType === "Income"){
				searchDataArray = searchData.filter(name => {
					// console.log(name)
					return name.type === "Income"
				})
				setList(searchDataArray.map(transaction => {
					return(
						<React.Fragment key={transaction._id}>
							<Card.Header>{transaction.categoryName} ({transaction.type})</Card.Header>
							<Card.Body>
							    <p>Added Amount: {transaction.amount}</p>
							    <p>Description: {transaction.description}</p>
							    <p>Balance: &#8369; {transaction.balanceAfterTransaction}</p>
							    <p>Transaction Date: {moment(transaction.dateAdded).format('LLL')}</p>
							</Card.Body>
						</React.Fragment>
					)
				}))
			}else{
				searchDataArray = searchData.filter(name => {
					// console.log(name)
					return name.type === "Expenses"
				})
				setList(searchDataArray.map(transaction => {
					return(
						<React.Fragment key={transaction._id}>
							<Card.Header>{transaction.categoryName} ({transaction.type})</Card.Header>
							<Card.Body>
							    <p>Amount Spent: {transaction.amount}</p>
							    <p>Description: {transaction.description}</p>
							    <p>Balance: &#8369; {transaction.balanceAfterTransaction}</p>
							    <p>Transaction Date: {moment(transaction.dateAdded).format('LLL')}</p>
							</Card.Body>
						</React.Fragment>
					)
				}))
			}
		}else{
    		setList(() => {
    			return(
					<React.Fragment>
						<Card.Header></Card.Header>
						<Card.Body>
						    <p>No Record</p>
						</Card.Body>
					</React.Fragment>
				)
    		})
    	}
    }, [searchData])

	return(
		<React.Fragment>
			<Card className="mb-3">
				<Card.Body>
					<Form>
					  <Form.Group controlId="categoryType">
					    <Form.Control as="select" onChange={e => setCategoryType(e.target.value)}>
					    	  <option value="All">All</option>
				              <option value="Income">Income</option>
				              <option value="Expenses">Expenses</option>
					    </Form.Control>
					  </Form.Group>
					  <Form.Group>
				      	<FormControl type="text" placeholder="Search" className="mr-sm-2" value={search} onChange={e => setSearch(e.target.value)}/>
				      </Form.Group>
					</Form>
					<Card className="mt-2">
				  		{list}	
					</Card>
				</Card.Body>
			</Card>			
		</React.Fragment>
	)
}