import { useState, useEffect } from 'react';
import { Button, Form, Card } from 'react-bootstrap'
import Router from 'next/router'
import AppHelper from '../apphelper'

export default function transactions(){

    const [catName, setCatName] = useState('')
    const [catType, setCatType] = useState('Income')
    const [amount, setAmount] = useState(0)
    const [note, setNote] = useState('')
    const [catNameData, setCatNameData] = useState('')

    useEffect(() => {
    	fetch(`${ AppHelper.API_URL }/users/get-categories`, {
    		method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			console.log(data[0].name)

			const types = data.filter(type => type.type === "Expenses")

			console.log(types)

			let categoryArr;

			if(data.length > 0){
				if(catType === "Income"){
					categoryArr = data.filter(type => {
						console.log(type)
						return type.type === "Income"
					})
					setCatNameData(categoryArr.map(category => {
						return <option key={category._id} value={category.name}>{category.name}</option>
					}))
				}else{
					categoryArr = data.filter(type => {
						console.log(type.type)
						return type.type === "Expenses"
					})
					setCatNameData(categoryArr.map(category => {
						return <option key={category._id} value={category.name}>{category.name}</option>
					}))
				}
				console.log(categoryArr.length === 0)

				if(categoryArr.length === 0){
					alert("Please Create Income/Expense Category!")
					Router.push('/addcategory')
				}else{
					setCatName(categoryArr[0].name)
				}
			}else{
				alert("Please Create Transactions!")
			}
		})
    }, [catType])
		
    function createNewRecord(e){
    	e.preventDefault();

    	console.log(catName)
    	console.log(catType)
    	console.log(amount)
    	console.log(note)

    	fetch(`${ AppHelper.API_URL }/users/add-record`, {
    		method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				categoryName: catName,
				typeName: catType,
				amount: amount,
				description: note
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true){
                Router.push('/categories')
            }else{
                Router.push('/error')
            }
		})
    }

	return(
		<React.Fragment>
			<Card>
				<Card.Body>
					<Form onSubmit={(e) => createNewRecord(e)}>
					  <Form.Group controlId="catType">
					    <Form.Label>Category Type</Form.Label>
					    <Form.Control as="select" onChange={e => setCatType(e.target.value)}>
				              <option value="Income">Income</option>
				              <option value="Expenses">Expenses</option>
					    </Form.Control>
					  </Form.Group>
					  <Form.Group controlId="catName">
					    <Form.Label>Category Name</Form.Label>
					    <Form.Control as="select" onChange={e => setCatName(e.target.value)}>
				              {catNameData}
					    </Form.Control>
					  </Form.Group>
					  <Form.Group controlId="exampleForm.ControlInput1">
					    <Form.Label>Amount</Form.Label>
					    <Form.Control type="number" value={amount} onChange={e => setAmount(e.target.value)} required/>
					  </Form.Group>
					  <Form.Group controlId="note">
					    <Form.Label>Note</Form.Label>
					    <Form.Control as="textarea" rows={3} value={note} onChange={e => setNote(e.target.value)}/>
					  </Form.Group>
					  <Button variant="primary" type="submit">Submit</Button>
					</Form>
				</Card.Body>
			</Card>
		</React.Fragment>
	)
}