import { useState, useEffect } from 'react'
import { Card, Table, Button, Form, Tabs, Tab } from 'react-bootstrap'
import Router from 'next/router'
// import { useRouter } from 'next/router'
import AppHelper from '../apphelper'
import Swal from 'sweetalert2'
import welcomes from '../styles/Welcome.module.css'

export default function categories(){
	// const router = useRouter()
	// const catId = router.query.catId
    const [list, setList] = useState('')
    const [categoryList, setCategoryList] = useState('')

    const [catName, setCatName] = useState('')
    const [catType, setCatType] = useState('Income')    

    useEffect(() => {
    	fetch(`${ AppHelper.API_URL }/users/get-categories`, {
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCategoryList(data)
			if(categoryList.length > 0){
				setList(categoryList.map(category =>{
					return(
						<React.Fragment>	
							<tr>
						      <td>{category.type}</td>
						      <td>{category.name}</td>
						    </tr>
						</React.Fragment>
					)
				}))
			}else{
				setList(() => {
					return(
						<React.Fragment>	
							<tr>
						      <td>No Records.</td>
						      <td></td>
						    </tr>
						</React.Fragment>
					)
				})
			}
		})
    }, [categoryList])

    function createNewCategory(e){
    	e.preventDefault();

    	console.log(catName)
    	console.log(catType)

    	fetch(`${ AppHelper.API_URL }/users/add-category`, {
    		method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: catName,
				typeName: catType
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire({
				  position: 'center',
				  icon: 'success',
				  title: 'A new Category was successfully added!',
				  showConfirmButton: false,
				  timer: 1500
				})
                Router.push('/categories')
            }else{
                Swal.fire({
				  icon: 'error',
				  title: 'Oops...',
				  text: 'Something went wrong!'
				})
            }
		})

		setCatName('')
    }    

	return(
		<React.Fragment>
			<body className={welcomes.welcome}>
			<Card>
				<Card.Body>
					<Tabs defaultActiveKey="categories" id="monthlyFigures" className="mb-3">
						<Tab eventKey="categories" title="List of Categories">
							<Table striped bordered hover size="sm">
							  <thead>
							    <tr className="text-center">
							    	<th>Category Type</th>
							    	<th>Category Name</th>
							    </tr>
							  </thead>
							  <tbody>
							    {list}
							  </tbody>
							</Table>
						</Tab>
						<Tab eventKey="addcategories" title="Add Category">
							<Form onSubmit={(e) => createNewCategory(e)}>
							  <Form.Group controlId="catName">
							    <Form.Label>Category Name</Form.Label>
							    <Form.Control type="text" placeholder="Enter Category Name" value={catName} onChange={e => setCatName(e.target.value)} required/>
							  </Form.Group>
							  <Form.Group controlId="catType">
							    <Form.Label>Type Name</Form.Label>
							    <Form.Control as="select" onChange={e => setCatType(e.target.value)}>
						              <option value="Income">Income</option>
							      	  <option value="Expenses">Expenses</option>
							    </Form.Control>
							  </Form.Group>
							  <Button variant="primary" type="submit">Submit</Button>
							</Form>
						</Tab>
					</Tabs>
				</Card.Body>
			</Card>	
			</body>		
		</React.Fragment>
	)
}