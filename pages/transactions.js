import { useState, useEffect } from 'react';
import { Card, Form, FormControl, Button, Tabs, Tab, Row, Col } from 'react-bootstrap'
import Router from 'next/router'
import AppHelper from '../apphelper'
import moment from 'moment'
import Swal from 'sweetalert2'
import transacts from '../styles/Transaction.module.css'

export default function transactions(){
    const [catName, setCatName] = useState('')
    const [catType, setCatType] = useState('Income')
    const [amount, setAmount] = useState(0)
    const [note, setNote] = useState('')
    const [catNameData, setCatNameData] = useState('')

    const [categories, setCategories] = useState([])
    const [categoryType, setCategoryType] = useState('All')
    const [list, setList] = useState([])
    const [search, setSearch] = useState('')
    const [searchData, setSearchData] = useState('')

    useEffect(() => {
    	fetch(`${ AppHelper.API_URL }/users/get-categories`, {
    		method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			let categoryArr = [];

			if(data.length > 0){
				if(catType === "Income"){
					categoryArr = data.filter(type => {
						console.log(type)
						return type.type === "Income"
					})
					setCatNameData(categoryArr.map(category => {
						return <option key={category._id} value={category.name}>{category.name}</option>
					}))
				}else{
					categoryArr = data.filter(type => {
						console.log(type.type)
						return type.type === "Expenses"
					})
					setCatNameData(categoryArr.map(category => {
						return <option key={category._id} value={category.name}>{category.name}</option>
					}))
				}
				console.log(categoryArr.length === 0)

				if(categoryArr.length === 0){
					Swal.fire('Oops...', 'Please Create Income/Expense Category!', 'warning')
					// alert("Please Create Income/Expense Category!")
					Router.push('/categories')
				}else{
					setCatName(categoryArr[0].name)
				}
			}else{
				Swal.fire('Oops...', 'Please Create Category!', 'warning')
			}
		})
    }, [catType])
		
    function createNewRecord(e){
    	e.preventDefault();

    	console.log(catName)
    	console.log(catType)
    	console.log(amount)
    	console.log(note)

    	fetch(`${ AppHelper.API_URL }/users/add-record`, {
    		method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				categoryName: catName,
				typeName: catType,
				amount: amount,
				description: note
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire({
				  position: 'center',
				  icon: 'success',
				  title: 'Transaction has been added!',
				  showConfirmButton: false,
				  timer: 1500
				})
                Router.push('/transactions')
            }else{
                Swal.fire({
				  icon: 'error',
				  title: 'Oops...',
				  text: 'Something went wrong!'
				})
            }
		})

		setAmount(0)
		setNote('')
    }

    useEffect(() => {
    	fetch(`${ AppHelper.API_URL }/users/get-records`, {
    		method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.length > 0){
				data.reverse()
				setCategories(data)
			}else{
				Swal.fire('Oops...', 'Please Create Transactions!', 'warning')				
			}
		})
    }, [categories])

    useEffect(() => {
    	if(search !== ""){
    		let term = new RegExp(search.toLowerCase())

    		setSearchData(categories.filter(record => {
    			return (term.test(record.categoryName.toLowerCase())
    				|| term.test(record.description.toLowerCase())
    				|| term.test(record.type.toLowerCase())
    				|| term.test(record.amount.toString())
    				|| term.test(record.balanceAfterTransaction.toString())
    				|| term.test(record.dateAdded.toString()))
    		}))
    	}else{
    		setSearchData(categories)
    	}
    }, [search, categories])

    useEffect(() => {
    	let searchDataArray = [];
    	if(searchData.length > 0){
			if(categoryType === "All"){
				searchDataArray = searchData.filter(name => {
					console.log(name)
					return name.type === name.type
				})
				setList(searchDataArray.map(transaction => {
					return(
						<React.Fragment key={transaction._id}>
							<Card.Header>{transaction.categoryName} ({transaction.type})</Card.Header>
							<Card.Body>
								{transaction.type === "Income" 
								? <p>Added Amount: {transaction.amount}</p> 
								: <p>Amount Spent: {transaction.amount}</p>}
							    <p>Balance: &#8369; {transaction.balanceAfterTransaction}</p>
							    <p>Description: {transaction.description}</p>
							    <p>Transaction Date: {moment(transaction.dateAdded).format('LLL')}</p>
							</Card.Body>
						</React.Fragment>
					)
				}))
			}else if(categoryType === "Income"){
				searchDataArray = searchData.filter(name => {
					// console.log(name)
					return name.type === "Income"
				})
				setList(searchDataArray.map(transaction => {
					return(
						<React.Fragment key={transaction._id}>
							<Card.Header>{transaction.categoryName} ({transaction.type})</Card.Header>
							<Card.Body>
							    <p>Added Amount: {transaction.amount}</p>
							    <p>Balance: &#8369; {transaction.balanceAfterTransaction}</p>
							    <p>Description: {transaction.description}</p>
							    <p>Transaction Date: {moment(transaction.dateAdded).format('LLL')}</p>
							</Card.Body>
						</React.Fragment>
					)
				}))
			}else{
				searchDataArray = searchData.filter(name => {
					// console.log(name)
					return name.type === "Expenses"
				})
				setList(searchDataArray.map(transaction => {
					return(
						<React.Fragment key={transaction._id}>
							<Card.Header>{transaction.categoryName} ({transaction.type})</Card.Header>
							<Card.Body>
							    <p>Amount Spent: {transaction.amount}</p>
							    <p>Balance: &#8369; {transaction.balanceAfterTransaction}</p>
							    <p>Description: {transaction.description}</p>
							    <p>Transaction Date: {moment(transaction.dateAdded).format('LLL')}</p>
							</Card.Body>
						</React.Fragment>
					)
				}))
			}
		}else{
    		setList(() => {
    			return(
					<React.Fragment>
						<Card.Header></Card.Header>
						<Card.Body>
						    <p>No Record</p>
						</Card.Body>
					</React.Fragment>
				)
    		})
    	}
    }, [searchData])

	return(
		<React.Fragment>
            <body className={transacts.transact}>		
			<Card className="mb-3">
				<Card.Body>
					<Tabs defaultActiveKey="transactions" id="monthlyFigures" className="mb-3">
						<Tab eventKey="transactions" title="Transactions">
							<Form>
								<Row>
									<Col className="col-7">
									  <Form.Group controlId="categoryType">
									    <Form.Control as="select" onChange={e => setCategoryType(e.target.value)}>
									    	  <option value="All">All</option>
								              <option value="Income">Income</option>
								              <option value="Expenses">Expenses</option>
									    </Form.Control>
									  </Form.Group>
									</Col>
									<Col className="col-5">
									  <Form.Group>
								      	<FormControl type="text" placeholder="Search" className="mr-sm-2" value={search} onChange={e => setSearch(e.target.value)}/>
								      </Form.Group>
								    </Col>
							    </Row>
							</Form>
							<Card>
						  		{list}	
							</Card>
						</Tab>
						<Tab eventKey="addrecord" title="Add Transaction">
							<Form onSubmit={(e) => createNewRecord(e)}>
							  <Form.Group controlId="catType">
							    <Form.Label>Category Type</Form.Label>
							    <Form.Control as="select" onChange={e => setCatType(e.target.value)}>
						              <option value="Income">Income</option>
						              <option value="Expenses">Expenses</option>
							    </Form.Control>
							  </Form.Group>
							  <Form.Group controlId="catName">
							    <Form.Label>Category Name</Form.Label>
							    <Form.Control as="select" onChange={e => setCatName(e.target.value)}>
						              {catNameData}
							    </Form.Control>
							  </Form.Group>
							  <Form.Group controlId="amount">
							    <Form.Label>Amount</Form.Label>
							    <Form.Control type="number" value={amount} onChange={e => setAmount(e.target.value)} required/>
							  </Form.Group>
							  <Form.Group controlId="note">
							    <Form.Label>Note</Form.Label>
							    <Form.Control as="textarea" rows={3} value={note} onChange={e => setNote(e.target.value)}/>
							  </Form.Group>
							  <Button variant="primary" type="submit">Submit</Button>
							</Form>
						</Tab>
					</Tabs>				
				</Card.Body>
			</Card>
			</body>
		</React.Fragment>
	)
}